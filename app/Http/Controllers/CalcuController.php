<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalcuController extends Controller
{
    public function index(Request $request){
        $valueone = $request->input('valueone');
        $valuetwo = $request->input('valuetwo');
        $valuethree = $request->input('valuethree');
        
    
        $rectangle = $valueone * $valuetwo;
        $cuboid = $valuetwo * $valuetwo * $valuethree;
        $square = $valueone * $valueone;
        
        $results = array($rectangle, $cuboid, $square);

            
        // echo $result;
        return view('Calculator',['results' => $results]);
}
}
